# Simple bash monitor for Linux se.vruntime parameter used by CFS scheduler
Bash program to monitor se.vruntime for a specified sub-process of init/systemd
Can be extended to use in many other cases.